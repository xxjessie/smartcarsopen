from django.conf.urls import patterns, url
from clientapp import views
urlpatterns = patterns('',
    url(r'^$',views.index,name='index'),
    url(r'^request/$',views.request,name='request'), 
    url(r'^update/$',views.update,name='update'),
    url(r'^end/$',views.end,name='end'),
)
