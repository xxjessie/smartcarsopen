from django.shortcuts import render
from django.contrib.gis.geos import *
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.contrib.gis.geos import fromstr
from django.core import serializers
import json
from django.http import HttpResponse
from clientapp.models import CarIdentity,CarLoc

# Create your views here.
def index(request):
    return render(request,'clientapp/index.html')

def request(request):
    sucess = False
    to_return = {'msg':'none'}
    if request.method == "POST":
        post = request.POST.copy()
    if post.has_key('name'):
        name = post['name']
        cars = CarIdentity.objects.filter(name = name)
        if cars.count() > 1:
            raise Exception("MORE THAN ONE CAR WITH NAME + " + name)
        if cars.count()>0:
            carLoc = CarLoc.objects.filter(car_identity = cars[0].id)
            to_return = {'msg':'duplicate car', 'id': cars[0].id, 'carObject': serializers.serialize("json", carLoc)}
            # serialized = serializers.serialize("json", carLoc)
            serialized = json.dumps(to_return)
            print serialized
        else:
            new_car = CarIdentity(name = name)
            new_car.save()
            new_point = Point(-73.935,40.73)
            new_loc = new_car.carloc_set.create(prev_lat = 100,prev_lng = 100,speed = 10,location = new_point)
 #           new_loc = new_car.carloc_set.create(prev_lat = 100,prev_lng = 100,speed = 10)
            new_loc.save()
            to_return = {'msg': "new id provided = " + str(new_car.id), 'id': new_car.id}
            serialized = json.dumps(to_return)

    return HttpResponse(serialized,content_type = "application/json")

def update(request):
    sucess = False
    if request.method == "POST":
        print "update called"
        post = request.POST.copy()
        if post.has_key('id'):
            id = post['id']
            print id
            if CarIdentity.objects.filter(id = id).count() > 0:
                temp = CarIdentity.objects.get(id = id)
                loc = temp.carloc_set.all()[0]
                loc.prev_lat = post['prev_lat']
                loc.prev_lng = post['prev_lng']
                loc.speed = post['speed']
                lat=post['now_lng']
                lng=post['now_lat']
                lat_lng=post['now_lng']+" "+post['now_lat']
                #print loc.prev_lat
                point = fromstr('POINT('+lat_lng+')')
                loc.location = point
                loc.save()
                qs = CarLoc.objects.all()
                qs = CarLoc.objects.filter(location__distance_gte=(point,D(km=0.001)))
                print "found qs"
                if len(qs) == 0:
                    print "no near cars"
                else:
                    print "length of qs = " + str(len(qs))
                print qs

                #print loc
                
                print "location updated"
                data = serializers.serialize("json",qs)
                to_return = {'msg':'id removed'}
            else:
                to_return = {'msg':'supplied id does not exist in database'}
        else:
            to_return = {'msg':'request does not have id'}
    serialized = json.dumps(to_return)
    print to_return
    return HttpResponse(data,content_type = "application/json")

def end(request):
    sucess = False
    to_return = {'msg':'none'}
    if request.method == "POST":
        post = request.POST.copy()
        if post.has_key('id'):
            id = post['id']
            print id
            if CarIdentity.objects.filter(id = id).count() > 0:
                temp = CarIdentity.objects.get(id = id)
                temp.delete()
                to_return = {'msg':'id removed'}
            else:
                to_return = {'msg':'supplied id does not exist in database'}
        else:
            to_return = {'msg':'request does not have id'}
    serialized = json.dumps(to_return)
    print to_return
    return HttpResponse(serialized,content_type = "application/json")

