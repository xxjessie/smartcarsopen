from django.contrib import admin
from clientapp.models import CarIdentity, CarLoc


admin.site.register(CarIdentity)
admin.site.register(CarLoc)

# Register your models here.
